package com.five9;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.UUID;

public class ReadFromKafka
{
    public static void main( String[] args )
    {
        if ( args.length < 2 ) {
            System.err.println("Please provide bootstrap servers and topic to consume from.");
            return;
        }

        String bootstrapServers = args[0];
        String topicName = args[1];

        KafkaConsumer<String, byte[]> kafkaConsumer = new KafkaConsumer<>(getConsumerProperties(bootstrapServers));
        kafkaConsumer.subscribe(Collections.singletonList(topicName));

        try {
            while ( true ) {
                ConsumerRecords<String, byte[]> records = kafkaConsumer.poll(Duration.ofMillis(Long.MAX_VALUE));
                if ( records.isEmpty() ) {
                    continue;
                }

                for ( ConsumerRecord<String, byte[]> record : records ) {
                    System.out.println(new String(record.value()) + "," + System.currentTimeMillis());
                }
            }
        } finally {
            kafkaConsumer.close();
        }
    }

    private static Properties getConsumerProperties( String bootstrapServers )
    {
        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        props.put("group.id", UUID.randomUUID().toString());
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");

        return props;
    }
}
