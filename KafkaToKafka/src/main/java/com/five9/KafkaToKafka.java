package com.five9;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.io.kafka.KafkaIO;
import org.apache.beam.sdk.io.kafka.KafkaRecord;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;

/**
 * An template that copies messages from one Kafka topic to another Kafka topic.
 */
public class KafkaToKafka
{
    /**
     * Options supported by {@link KafkaToKafka}.
     *
     * <p>Inherits standard configuration options.
     */
    public interface Options
            extends PipelineOptions, StreamingOptions
    {
        @Description("Bootstrap servers for kafka")
        @Validation.Required
        String getBootstrapServers();

        void setBootstrapServers( String bootstrapServers );

        @Description("The Kafka topic to consume from.")
        @Validation.Required
        String getInputTopic();

        void setInputTopic( String inputTopic );

        @Description("The Kafka topic to publish to. ")
        @Validation.Required
        String getOutputTopic();

        void setOutputTopic( String outputTopic );
    }

    /**
     * DoFn that will add a timestamp to the message.
     */
    public static class AddTimestampFn
            extends DoFn<KafkaRecord<String, byte[]>, KV<String, byte[]>>
    {
        @ProcessElement
        public void processElement( ProcessContext context )
        {
            context.output(KV.of("",
                    (new String(context.element().getKV().getValue()) + "," + System.currentTimeMillis()).getBytes()));
        }
    }

    /**
     * Main entry point for executing the pipeline.
     *
     * @param args The command-line arguments to the pipeline.
     */
    public static void main( String[] args )
    {

        // Parse the user options passed from the command-line
        Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);

        options.setStreaming(true);

        run(options);
    }

    /**
     * Runs the pipeline with the supplied options.
     *
     * @param options The execution parameters to the pipeline.
     * @return The result of the pipeline execution.
     */
    private static PipelineResult run( Options options )
    {
        // Create the pipeline
        Pipeline pipeline = Pipeline.create(options);
        options.setJobName("KafkaToKafka");

        /*
         * Steps: 1) Read message from kafka as a String from input Kafka topic.
         *        2) Add a timestamp to the message payload
         *        3) Write each message as a String to output Kafka topic.
         */
        String bootstrapServers = options.getBootstrapServers();
        pipeline.apply("Read Kafka Events", KafkaIO.<String, byte[]>read().withBootstrapServers(bootstrapServers)
                .withTopics(new ArrayList<>((Collections.singletonList(options.getInputTopic()))))
                .withKeyDeserializer(StringDeserializer.class)
                .withValueDeserializer(ByteArrayDeserializer.class))
                .apply("Add timestamp", ParDo.of(new AddTimestampFn()))
                .apply("Write Kafka Events", KafkaIO.<String, byte[]>write().withBootstrapServers(bootstrapServers)
                        .withTopic(options.getOutputTopic())
                        .withKeySerializer(StringSerializer.class)
                        .withValueSerializer(ByteArraySerializer.class));

        // Execute the pipeline and return the result.
        return pipeline.run();
    }
}
