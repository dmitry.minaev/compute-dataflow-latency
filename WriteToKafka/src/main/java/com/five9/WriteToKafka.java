package com.five9;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class WriteToKafka
{
    private static final Logger LOG = LoggerFactory.getLogger(WriteToKafka.class);

    public static void main( String[] args )
    {
        if ( args.length < 3 ) {
            System.err.println("Please provide bootstrap servers, topic name and a number of messages to publish.");
            return;
        }

        String bootstrapServers = args[0];
        String topic = args[1];
        long count = Long.parseLong(args[2]);

        try (KafkaProducer<String, byte[]> kafkaProducer = new KafkaProducer<>(
                getProducerProperties(bootstrapServers))) {

            for ( int i = 0; i < count; i++ ) {

                String key = "";
                String message = i + "," + System.currentTimeMillis();
                byte[] value = message.getBytes();

                kafkaProducer.send(new ProducerRecord<>(topic, key, value));
                kafkaProducer.flush();
            }
        }
    }

    private static Properties getProducerProperties( String bootstrapServers )
    {
        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");

        return props;
    }

}
