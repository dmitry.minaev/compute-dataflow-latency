package com.five9;

import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;

public class ReadFromPubsub
{
    public static void main( String[] args )
    {
        if ( args.length < 2 ) {
            System.err.println("Please provide projectId and subscriptionId.");
            return;
        }

        String projectId = args[0];
        String subscriptionId = args[1];

        ProjectSubscriptionName subscriptionName = ProjectSubscriptionName.of(projectId, subscriptionId);
        // Instantiate an asynchronous message receiver
        MessageReceiver receiver = new MessageReceiver()
        {
            @Override
            public void receiveMessage( PubsubMessage message, AckReplyConsumer consumer )
            {
                consumer.ack();

                // handle incoming message, then ack/nack the received message
                long currentTimeMillis = System.currentTimeMillis();
                String stringMessage = message.getData().toStringUtf8();
                System.out.println(stringMessage + "," + currentTimeMillis);
            }
        };

        Subscriber subscriber = null;
        try {
            // Create a subscriber for "my-subscription-id" bound to the message receiver
            subscriber = Subscriber.newBuilder(subscriptionName, receiver).build();
            subscriber.startAsync();
            // ...
            for ( ; ; ) {
                Thread.sleep(Long.MAX_VALUE);
            }
        } catch ( InterruptedException e ) {
            System.err.println(e.getMessage());
        } finally {
            // stop receiving messages
            if ( subscriber != null ) {
                subscriber.stopAsync();
            }
        }
    }
}
