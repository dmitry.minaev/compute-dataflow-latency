# Compute latency for Google Dataflow

## Conclusion
I ran a couple of experiments with Google Dataflow working with Pubsub and Kafka.
My observations show that Dataflow with Pubsub produces an unacceptably high latency whether Dataflow with Kafka gives a good performance even on a smallest Kafka installation.

## Latency with Pubsub

### Installation instructions
- run `./gradlew clean build` in this project folder
- run Dataflow job, e.g. `java -jar PubsubToPubsub/build/libs/PubsubToPubsub-1.0-SNAPSHOT-all.jar --inputSubscription=projects/five9dataservices/subscriptions/experiments-string_events --outputTopic=projects/five9dataservices/topics/string_results --project=five9dataservices --runner=DataflowRunner --autoscalingAlgorithm=THROUGHPUT_BASED --maxNumWorkers=3`
- Create a new Compute engine instance in GCP (for producer and consumer)
- add a public ssh key to the compute engine to be able to ssh into it from your local console
- copy authentication json file to the remote server, e.g. `scp /path/to/my-auth.json <username>@<hostname>:~/`
- copy ReadFromPubsub-1.0-SNAPSHOT-all.jar to the remote server, e.g. `scp ReadFromPubsub/build/libs/ReadFromPubsub-1.0-SNAPSHOT-all.jar <username>@<hostname>:~/`
- copy WriteToPubsub-1.0-SNAPSHOT-all.jar to the remote server, e.g. `scp WriteToPubsub/build/libs/WriteToPubsub-1.0-SNAPSHOT-all.jar <username>@<hostname>:~/`
- ssh to the server
- install java8: `sudo apt-get -y install default-jdk`
- export env: `export GOOGLE_APPLICATION_CREDENTIALS="/home/<username>/my-auth.json"`
- run reader: `java -jar ReadFromPubsub-1.0-SNAPSHOT-all.jar five9dataservices experiments-string_results`
- run writer: `java -jar WriteToPubsub-1.0-SNAPSHOT-all.jar five9dataservices string_events 1`

### Results
I ran the test for a half an hour producing about 10,000 msg/sec. It produced about 1Gb of data in csv format. 
I uploaded it into Google Cloud storage and imported into BigQuery (table `latency.dataflow_latency`):
```
Field name	Type	Mode	    Description
id	        INTEGER	REQUIRED	
producer_ts	NUMERIC	NULLABLE	
dataflow_ts	NUMERIC	NULLABLE	
consumer_ts	NUMERIC	NULLABLE
```

Then I ran the following query:
```
SELECT
 	FORMAT_TIMESTAMP('%H:%M', TIMESTAMP_SECONDS(CAST(DIV(producer_ts/1000, 60*5)*60*5 as INT64))) interval_5min,
  avg((dataflow_ts-producer_ts)/1000) avg_df_latency_sec,
	avg((consumer_ts-dataflow_ts)/1000) avg_consumer_latency_sec,
	avg((consumer_ts-producer_ts)/1000) avg_overall_latency_sec
from latency.dataflow_latency
group by interval_5min
```

and it produced the following results:
```
interval_5min	avg_df_latency_sec	avg_consumer_latency_sec	avg_overall_latency_sec
1:30	        4.662352386	        2.423224546	                7.085576932
1:35	        4.650358175	        2.305476103	                6.955834278
1:40	        4.560882435	        2.402289772	                6.963172207
1:45	        4.540845529	        2.463710164	                7.004555693
1:50	        4.594879487	        2.414882388	                7.009761875
1:55	        4.523008458	        2.407590616	                6.930599073
2:00	        4.612696639	        2.404771252	                7.017467891
2:05	        4.710669389	        2.421303651	                7.13197304
```

where: 
- `avg_df_latency_sec` - average number of seconds (within 5 minute interval) it takes for Google Dataflow to consume messages from Pubsub.
More formally it's a duration between the time when producer generated event and Dataflow consumed it.
- `avg_consumer_latency_sec` - average number of seconds (within 5 minute interval) it takes for consumer to get the data from Pubsub.
More formally it's a duration between the time when Dataflow emitted event and consumer consumed it.
- `avg_overall_latency_sec` = `avg_df_latency_sec` + `avg_consumer_latency_sec`

__As you can see the latency is pretty high. I'm thinking it may be due to some configuration that needs to be applied that I'm not aware of. My plan is to contact Google Support representative for help.__ 

## Latency with Kafka

### Installation instructions
- run `./gradlew clean build` in this project folder
- run Dataflow job, e.g. `java -jar KafkaToKafka/build/libs/KafkaToKafka-1.0-SNAPSHOT-all.jar --bootstrapServers=34.94.91.233:9092 --inputTopic=string_events --outputTopic=string_results --project=five9dataservices --runner=DataflowRunner --autoscalingAlgorithm=THROUGHPUT_BASED --maxNumWorkers=3`
- Install kafka on GCP, e.g. using [Kafka (Google Click to Deploy)](https://console.cloud.google.com/marketplace/details/click-to-deploy-images/kafka?q=kafka&id=f19a0f63-fc57-47fd-9d94-8d5ca6af935e&project=five9dataservices)
- Open a page with kafka instance and create a new network tag with Ingress, open ports: tcp:2181,2888,3888,8081-8083,9092,9581-9585,3030,3031
- Assign this tag to the kafka instance
- Ssh to kafka instance and update /opt/kafka/config/server.properties, uncomment `advertised.listeners` and put an external ip address there
- Create a new Compute engine instance in GCP (for producer and consumer)
- add a public ssh key to the compute engine to be able to ssh into it from your local console
- copy authentication json file to the remote server, e.g. `scp /path/to/my-auth.json <username>@<hostname>:~/`
- copy ReadFromKafka-1.0-SNAPSHOT-all.jar to the remote server, e.g. `scp ReadFromKafka/build/libs/ReadFromKafka-1.0-SNAPSHOT-all.jar <username>@<hostname>:~/`
- copy WriteToKafka-1.0-SNAPSHOT-all.jar to the remote server, e.g. `scp WriteToKafka/build/libs/WriteToKafka-1.0-SNAPSHOT-all.jar <username>@<hostname>:~/`
- ssh to the server
- install java8: `sudo apt-get -y install default-jdk`
- export env: `export GOOGLE_APPLICATION_CREDENTIALS="/home/<username>/my-auth.json"`
- run reader: `java -jar ReadFromKafka-1.0-SNAPSHOT-all.jar <kafka_ip_address>:9092 string_results`
- run writer: `java -jar WriteToKafka-1.0-SNAPSHOT-all.jar <kafka_ip_address>:9092 string_events 1000`

### Results
I ran the test for a half an hour producing about 2,000 msg/sec. Seems like producing to Kafka is slower in this small configuration than producing to Pubsub with production-like configuration.
But I made another test producing 2,000 msg/sec to Pubsub to be able to accurately compare the results. Unfortunately, the latency is very close to the numbers for 10,000 msg/sec which means it's too high.  
I uploaded it into Google Cloud storage and imported into BigQuery (table `latency.dataflow_kafka_latency`):
```
Field name	Type	Mode	    Description
id	        INTEGER	REQUIRED	
producer_ts	NUMERIC	NULLABLE	
dataflow_ts	NUMERIC	NULLABLE	
consumer_ts	NUMERIC	NULLABLE
```

Then I ran the following query:
```
SELECT
 	FORMAT_TIMESTAMP('%H:%M', TIMESTAMP_SECONDS(CAST(DIV(producer_ts/1000, 60*5)*60*5 as INT64))) interval_5min,
  avg((dataflow_ts-producer_ts)/1000) avg_df_latency_sec,
	avg((consumer_ts-dataflow_ts)/1000) avg_consumer_latency_sec,
	avg((consumer_ts-producer_ts)/1000) avg_overall_latency_sec
from latency.dataflow_kafka_latency
group by interval_5min
```

and it produced the following results:
```
interval_5min	avg_df_latency_sec	avg_consumer_latency_sec	avg_overall_latency_sec
23:40	        0.165427146	        0.054032366	                0.219459512
23:45	        0.151178627	        0.05364902	                0.204827647
23:50	        0.164367838	        0.050616877	                0.214984714
23:55	        0.158352738	        0.047033881	                0.205386619
0:00	        0.165193409	        0.045504848	                0.210698257
0:05	        0.169291497	        0.045163544	                0.214455041
0:10	        0.16226973	        0.04446296	                0.206732689
0:15	        0.186723979	        0.045359946	                0.232083925
```

The results show that the latency on Dataflow and on consumer side is much lower with Kafka which is significantly 
better than with Pubsub. And that's with some very small kafka standalone server installation, 1 node machine type 
n1-standard-1 (1 vCPU, 3.75 GB memory), with a topic with 1 partition.
