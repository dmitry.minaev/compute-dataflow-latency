package com.five9;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An template that copies messages from one Pubsub subscription to another Pubsub topic.
 */
public class PubsubToPubsub
{
    /**
     * Options supported by {@link PubsubToPubsub}.
     *
     * <p>Inherits standard configuration options.
     */
    public interface Options
            extends PipelineOptions, StreamingOptions
    {
        @Description("The Cloud Pub/Sub subscription to consume from. " + "The name should be in the format of "
                + "projects/<project-id>/subscriptions/<subscription-name>.")
        @Validation.Required
        ValueProvider<String> getInputSubscription();

        void setInputSubscription( ValueProvider<String> inputSubscription );

        @Description("The Cloud Pub/Sub topic to publish to. " + "The name should be in the format of "
                + "projects/<project-id>/topics/<topic-name>.")
        @Validation.Required
        ValueProvider<String> getOutputTopic();

        void setOutputTopic( ValueProvider<String> outputTopic );
    }

    /**
     * DoFn that will add a timestamp to the message.
     */
    public static class AddTimestampFn
            extends DoFn<String, String>
    {
        private static final Logger LOG = LoggerFactory.getLogger(AddTimestampFn.class);

        @ProcessElement
        public void processElement( ProcessContext context )
        {
            context.output(context.element() + "," + System.currentTimeMillis());
        }
    }

    /**
     * Main entry point for executing the pipeline.
     *
     * @param args The command-line arguments to the pipeline.
     */
    public static void main( String[] args )
    {

        // Parse the user options passed from the command-line
        Options options = PipelineOptionsFactory.fromArgs(args).withValidation().as(Options.class);

        options.setStreaming(true);

        run(options);
    }

    /**
     * Runs the pipeline with the supplied options.
     *
     * @param options The execution parameters to the pipeline.
     * @return The result of the pipeline execution.
     */
    private static PipelineResult run( Options options )
    {
        // Create the pipeline
        Pipeline pipeline = Pipeline.create(options);
        options.setJobName("PubsubToPubsub");

        /*
         * Steps: 1) Read PubSubMessage as a String from input PubSub subscription.
         *        2) Add a timestamp to the message payload
         *        3) Write each PubSubMessage as a String to output PubSub topic.
         */
        pipeline.apply("Read PubSub Events", PubsubIO.readStrings().fromSubscription(options.getInputSubscription()))
                .apply("Add timestamp", ParDo.of(new AddTimestampFn()))
                .apply("Write PubSub Events", PubsubIO.writeStrings().to(options.getOutputTopic()));

        // Execute the pipeline and return the result.
        return pipeline.run();
    }
}