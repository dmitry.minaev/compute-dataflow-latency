package com.five9;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.api.gax.batching.BatchingSettings;
import com.google.api.gax.rpc.ApiException;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;

import java.util.concurrent.TimeUnit;

public class WriteToPubsub
{
    private static void publishMessages( String... args ) throws Exception
    {
        String projectId = args[0];
        String topic = args[1];
        long count = Long.parseLong(args[2]);

        ProjectTopicName topicName = ProjectTopicName.of(projectId, topic);
        Publisher publisher = null;

        try {
            // Create a publisher instance with default settings bound to the topic
            publisher = Publisher.newBuilder(topicName)
                    .setBatchingSettings(BatchingSettings.newBuilder().setIsEnabled(false).build())
                    .build();

            for ( int i = 0; i < count; i++ ) {
                String message = "" + System.currentTimeMillis();

                // schedule publishing one message at a time : messages get automatically batched
                ByteString data = ByteString.copyFromUtf8(message);
                PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

                // Once published, returns a server-assigned message id (unique within the topic)
                ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
                System.out.println(messageIdFuture.get());
            }

        } finally {
            if ( publisher != null ) {
                // When finished with the publisher, shutdown to free up resources.
                publisher.shutdown();
                publisher.awaitTermination(1, TimeUnit.MINUTES);
            }
        }
    }

    private static void publishMessagesAsync( String... args ) throws Exception
    {
        String projectId = args[0];
        String topic = args[1];
        long count = Long.parseLong(args[2]);

        ProjectTopicName topicName = ProjectTopicName.of(projectId, topic);
        Publisher publisher = null;

        try {
            // Create a publisher instance with default settings bound to the topic
            publisher = Publisher.newBuilder(topicName)
                    .setBatchingSettings(BatchingSettings.newBuilder().setIsEnabled(false).build())
                    .build();

            for ( int i = 0; i < count; i++ ) {
                String message = "" + System.currentTimeMillis();

                // schedule publishing one message at a time : messages get automatically batched
                ByteString data = ByteString.copyFromUtf8(message);
                PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

                // Once published, returns a server-assigned message id (unique within the topic)
                ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);

                // Add an asynchronous callback to handle success / failure
                ApiFutures.addCallback(messageIdFuture, new ApiFutureCallback<String>()
                {
                    @Override
                    public void onFailure( Throwable throwable )
                    {
                        if ( throwable instanceof ApiException ) {
                            ApiException apiException = ((ApiException) throwable);
                            // details on the API exception
                            System.out.println(apiException.getStatusCode().getCode());
                            System.out.println(apiException.isRetryable());
                        }
                        System.out.println("Error publishing message : " + message);
                    }

                    @Override
                    public void onSuccess( String messageId )
                    {
                        // Once published, returns server-assigned message ids (unique within the topic)
                        System.out.println(messageId);
                    }
                }, MoreExecutors.directExecutor());
            }

        } finally {
            if ( publisher != null ) {
                // When finished with the publisher, shutdown to free up resources.
                publisher.shutdown();
                publisher.awaitTermination(1, TimeUnit.MINUTES);
            }
        }
    }

    public static void main( String... args ) throws Exception
    {
        // publishMessages(args);
        publishMessagesAsync(args);
    }
}